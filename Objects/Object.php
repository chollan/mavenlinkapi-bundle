<?php

namespace MavenlinkApiBundle\Objects;

class Object
{
    public static function path()
    {
        return static::$path;
    }

    public static function getResourcesPath()
    {
        return self::path() . ".json";
    }

    public static function getResourcePath($resourceId)
    {
        return self::path() . "/$resourceId" . ".json";
    }

    public static function getWorkspaceResourcePath($workspaceId, $resourceId)
    {
        return "workspaces/$workspaceId/" . self::path() . "/$resourceId.json";
    }

    public static function getWorkspaceResourcesPath($workspaceId)
    {
        return "workspaces/$workspaceId/" . self::path() . ".json";
    }

    public static function getWorkspacePermissionsPath($workspaceId)
    {
        return "workspaces/$workspaceId/" . self::path() . ".json";
    }
}