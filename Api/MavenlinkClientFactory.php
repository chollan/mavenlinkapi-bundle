<?php
/**
 * Created by PhpStorm.
 * User: cholland
 * Date: 12/4/18
 * Time: 5:36 PM
 */

namespace MavenlinkApiBundle\Api;


class MavenlinkClientFactory
{
    public static function createMavenlink($appURL, $app_id, $app_secret, $oauth_token){
        $mavenlink = new MavenlinkClient($appURL, $app_id, $app_secret);
        $mavenlink->setToken($oauth_token);
        return $mavenlink;
    }
}