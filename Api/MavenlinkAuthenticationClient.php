<?php

namespace MavenlinkApiBundle\Api;

class MavenlinkAuthenticationClient
{
    function __construct($appURL, $app_id, $app_secret)
    {
        $this->app_url = $appURL;
        $this->app_id = $app_id;
        $this->app_secret = $app_secret;
    }
}