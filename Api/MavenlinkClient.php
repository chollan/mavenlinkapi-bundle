<?php

namespace MavenlinkApiBundle\Api;

use MavenlinkApiBundle\Objects\Assignment;
use MavenlinkApiBundle\Objects\Event;
use MavenlinkApiBundle\Objects\Expense;
use MavenlinkApiBundle\Objects\Invitation;
use MavenlinkApiBundle\Objects\Invoice;
use MavenlinkApiBundle\Objects\Participant;
use MavenlinkApiBundle\Objects\Post;
use MavenlinkApiBundle\Objects\Story;
use MavenlinkApiBundle\Objects\StoryAllocationDay;
use MavenlinkApiBundle\Objects\TimeEntry;
use MavenlinkApiBundle\Objects\User;
use MavenlinkApiBundle\Objects\Workspace;
use MavenlinkApiBundle\Objects\WorkspaceAllocation;
use MavenlinkApiBundle\Objects\WorkspaceGroup;

class MavenlinkClient
{
    private $app_url;
    private $app_id;
    private $app_secret;
    private $loginInfo = null;
    private $models_namespace = 'MavenlinkApiBundle\\Objects\\';
    private $_retry_times = 3;
    private $_timeout_after = 10; // seconds
    private $_sleep_on_timeout = 25; // seconds
    private $_last_call_meta;

    function __construct($appURL, $app_id, $app_secret)
    {
        $this->app_url = $appURL;
        $this->app_id = $app_id;
        $this->app_secret = $app_secret;
    }

    function setToken($token){
        $this->loginInfo = $token;
    }

    function getAssignments($filters = array())
    {
        return $this->json2collection('Assignment', $this->getJsonForAll('Assignment', $filters));
    }
    
    function createAssignment($assignment){
        /* untedted */
        $workspaceAllocationParamsArray = $this->labelParamKeys('Assignment', $assignment);
        $newPath = $this->getBaseUri().Assignment::getResourcesPath();
        $curl = $this->createPostRequest($newPath, $this->loginInfo, $workspaceAllocationParamsArray);
        $response = $this->curlExec($curl);

        return $response;
    }

    function getStoryAllocatonDays($filters = array())
    {
        return $this->json2collection('StoryAllocationDay', $this->getJsonForAll('StoryAllocationDay', $filters));
    }

    function newStoryAllocationDay($allocation){
        $storyAllocationParamsArray = $this->labelParamKeys('StoryAllocationDay', $allocation);
        $newPath = $this->getBaseUri().StoryAllocationDay::getResourcesPath();
        $curl = $this->createPostRequest($newPath, $this->loginInfo, $storyAllocationParamsArray);
        $response = $this->curlExec($curl);

        return $response;
    }

    function getWorkspaceGroups($filters = array())
    {
        return $this->json2collection('WorkspaceGroup', $this->getJsonForAll('WorkspaceGroup', $filters));
    }

    function getWorkspaces($filters = array())
    {
        return $this->json2collection('Workspace', $this->getJsonForAll('Workspace', $filters));
    }

    function getWorkspaceAllocations($filters = array())
    {
        return $this->json2collection('WorkspaceAllocation', $this->getJsonForAll('WorkspaceAllocation', $filters));
    }

    function createWorkspaceAllocation($workspaceAllocationParamsArray){
        $workspaceAllocationParamsArray = $this->labelParamKeys('WorkspaceAllocation', $workspaceAllocationParamsArray);
        $newPath = $this->getBaseUri().WorkspaceAllocation::getResourcesPath();
        $curl = $this->createPostRequest($newPath, $this->loginInfo, $workspaceAllocationParamsArray);
        $response = $this->curlExec($curl);

        return $response;
    }

    function getEvents($filters = array())
    {
        return $this->json2collection('Event', $this->getJsonForAll('Event', $filters));
    }

    function getTimeEntries($filters = array())
    {
        return $this->json2collection('TimeEntry', $this->getJsonForAll('TimeEntry', $filters));
    }

    function getExpenses($filters = array())
    {
        return $this->json2collection('Expense', $this->getJsonForAll('Expense', $filters));
    }

    function getInvoices($filters = array())
    {
        return $this->json2collection('Invoice', $this->getJsonForAll('Invoice', $filters));
    }

    function getStories($filters = array())
    {
        return $this->json2collection('Story', $this->getJsonForAll('Story', $filters));
    }

    function getUsers($filters = array())
    {
        return $this->json2collection('User', $this->getJsonForAll('User', $filters));
    }

    function getTimeEntry($id)
    {
        return $this->getShowJsonFor('TimeEntry', $id);
    }

    function getExpense($id)
    {
        return $this->getShowJsonFor('Expense', $id);
    }

    function getInvoice($id)
    {
        return $this->getShowJsonFor('Invoice', $id);
    }

    function getStory($id)
    {
        return $this->getShowJsonFor('Story', $id);
    }

    function getWorkspace($id)
    {
        return $this->getShowJsonFor('Workspace', $id);
    }

    function createWorkspace($workspaceParamsArray)
    {
        $workspaceParamsArray = $this->labelParamKeys('Workspace', $workspaceParamsArray);
        $newPath = Workspace::getResourcesPath();
        $curl = $this->createPostRequest($newPath, $this->loginInfo, $workspaceParamsArray);
        $response = $this->curlExec($curl);

        return $response;
    }

    function updateWorkspace($workspaceId, $workspaceParamsArray)
    {
        $workspaceParamsArray = $this->labelParamKeys('Workspace', $workspaceParamsArray);

        $updatePath = Workspace::getResourcePath($workspaceId);
        $curl = $this->createPutRequest($updatePath, $this->loginInfo, $workspaceParamsArray);
        $response = $this->curlExec($curl);

        return $response;
    }

    function inviteToWorkspace($workspaceId, $invitationParamsArray)
    {
        return $this->createNewForWorkspace('Invitation', $workspaceId, $invitationParamsArray);
    }

    function getAllParticipantsFromWorkspace($workspaceId, $filters = array())
    {
        return $this->json2collection('User', $this->getJson(User::getResourcesPath() . "?participant_in=" . $workspaceId, $filters));
    }

    function getAllInvoicesFromWorkspace($workspaceId, $filters = array())
    {
        return $this->json2collection('Invoice', $this->getJson(Invoice::getResourcesPath() . "?workspace_id=" . $workspaceId, $filters));
    }

    function getWorkspaceInvoice($workspaceId, $invoiceId)
    {
        return $this->getJson(Invoice::getWorkspaceResourcePath($workspaceId, $invoiceId));
    }

    function getAllPostsFromWorkspace($workspaceId, $filters = array())
    {
        return $this->json2collection('Post', $this->getJson(Post::getResourcesPath() . "?workspace_id=" . $workspaceId, $filters));
    }

    function createPostForWorkspace($workspaceId, $postParamsArray)
    {
        return $this->createNew(Post, $workspaceId, $postParamsArray);
    }

    function getWorkspacePost($workspaceId, $postId)
    {
        return $this->getJson(Post::getWorkspaceResourcePath($workspaceId, $postId));
    }

    function updateWorkspacePost($workspaceId, $postId, $updateParams)
    {
        return $this->updateModel('Post', $workspaceId, $postId, $updateParams);
    }

    function deleteWorkspacePost($workspaceId, $postId)
    {
        return $this->deleteModel('Post', $workspaceId, $postId);
    }

    function getAllStoriesFromWorkspace($workspaceId, $filters = array())
    {
        return $this->json2collection('Story', $this->getJson(Story::getResourcesPath() . "?workspace_id=" . $workspaceId, $filters));
    }

    function createStoryForWorkspace($workspaceId, $storyParamsArray)
    {
        return $this->createNew('Story', $workspaceId, $storyParamsArray);
    }

    function getWorkspaceStory($workspaceId, $storyId)
    {
        return $this->getJson(Story::getWorkspaceResourcePath($workspaceId, $storyId));
    }

    function updateWorkspaceStory($workspaceId, $storyId, $updateParams)
    {
        return $this->updateModel('Story', $workspaceId, $storyId, $updateParams);
    }

    function deleteWorkspaceStory($workspaceId, $storyId)
    {
        return $this->deleteModel('Story', $workspaceId, $storyId);
    }

    function getAllTimeEntriesFromWorkspace($workspaceId, $filters = array())
    {
        return $this->json2collection('TimeEntry', $this->getJson(TimeEntry::getResourcesPath() . "?workspace_id=" . $workspaceId, $filters));
    }

    function createTimeEntryForWorkspace($workspaceId, $timeEntryParamsArray)
    {
        return $this->createNew('TimeEntry', $workspaceId, $timeEntryParamsArray);
    }

    function createTimesheetSubmissionForWorkspace($workspaceId, $timeSubmissionParamsArray)
    {
        return $this->createNew('TimesheetSubmission', $workspaceId, $timeSubmissionParamsArray);
    }

    function getWorkspaceTimeEntry($workspaceId, $timeEntryId)
    {
        return $this->getJson(TimeEntry::getWorkspaceResourcePath($workspaceId, $timeEntryId));
    }

    function updateWorkspaceTimeEntry($workspaceId, $timeEntryId, $updateParams)
    {
        return $this->updateModel('TimeEntry', $workspaceId, $timeEntryId, $updateParams);
    }

    function deleteWorkspaceTimeEntry($workspaceId, $timeEntryId)
    {
        return $this->deleteModel('TimeEntry', $workspaceId, $timeEntryId);
    }

    function getAllExpensesFromWorkspace($workspaceId, $filters = array())
    {
        return $this->json2collection('Expense', $this->getJson(Expense::getResourcesPath() . "?workspace_id=" . $workspaceId, $filters));
    }

    function createExpenseForWorkspace($workspaceId, $expenseParamsArray)
    {
        return $this->createNew('Expense', $workspaceId, $expenseParamsArray);
    }

    function getWorkspaceExpense($workspaceId, $expenseId)
    {
        return $this->getJson(Expense::getWorkspaceResourcePath($workspaceId, $expenseId));
    }

    function updateWorkspaceExpense($workspaceId, $expenseId, $updateParams)
    {
        return $this->updateModel('Expense', $workspaceId, $expenseId, $updateParams);
    }

    function deleteWorkspaceExpense($workspaceId, $expenseId)
    {
        return $this->deleteModel('Expense', $workspaceId, $expenseId);
    }

    public function updateStory($storyId, $updateParams)
    {
        return $this->updateModelObject('Story', $storyId, $updateParams);
    }

    public function updateTimeEntry($timeEntryId, $updateParams)
    {
        return $this->updateModelObject('TimeEntry', $timeEntryId, $updateParams);
    }

    function getJsonForAll($model, $filters = array())
    {
        $model = $this->getFullClassname($model);
        $resourcesPath = $this->getBaseUri().$model::getResourcesPath();
        return $this->getJson($resourcesPath, $filters);
    }

    function getShowJsonFor($model, $id)
    {
        $model = $this->getFullClassname($model);
        $resourcePath = $this->getBaseUri().$model::getResourcePath($id);
        return $this->getJson($resourcePath);
    }

    function getJson($path, $filters = array())
    {
        $path = $this->applyFilters($path, $filters);
        $curl = $this->getCurlHandle($path, $this->loginInfo);

        $json = $this->curlExec($curl);
        return $json;
    }

    function createNew($model, $workspaceId, $params)
    {
        $model = $this->getFullClassname($model);

        $params = $this->labelParamKeys($model, array_merge($params, array('workspace_id' => $workspaceId)));

        $newPath = $this->getBaseUri().$model::getResourcesPath();
        $curl = $this->createPostRequest($newPath, $this->loginInfo, $params);
        $response = $this->curlExec($curl);

        return $response;
    }

    function createNewForWorkspace($model, $workspaceId, $params)
    {
        $model = $this->getFullClassname($model);

        $params = $this->labelParamKeys($model, $params);

        $newPath = $this->getBaseUri().$model::getWorkspaceResourcesPath($workspaceId);
        $curl = $this->createPostRequest($newPath, $this->loginInfo, $params);
        $response = $this->curlExec($curl);

        return $response;
    }

    function wrapParamFor($model, $arrayKey)
    {
        $model = $this->getClassname($model);

        return strtolower(preg_replace('/(?<=\\w)(?=[A-Z])/', "_$1", "$model") . "[$arrayKey]");
    }

    function labelParamKeys($model, $paramsArray)
    {
        $model = $this->getFullClassname($model);
        $labelledArray = array();

        foreach ($paramsArray as $key => $value) {

            if ($this->keyAlreadyWrapped($model, $key)) {
                $wrappedKey = strtolower($key);
            } else {

                $wrappedKey = $this->wrapParamFor($model, $key);
            }

            $labelledArray[$wrappedKey] = $value;
        }

        return $labelledArray;
    }

    function keyAlreadyWrapped($object, $key)
    {
        $object = strtolower(preg_replace('/(?<=\\w)(?=[A-Z])/', "_$1", str_replace('\\', '\\\\', $object)));
        $matchPattern = "$object" . "\[\w+\]";
        $matchWrapped = 0;
        $matchWrapped = preg_match("/$matchPattern/", $key);

        return $matchWrapped == 1;
    }

    function updateModel($model, $workspaceId, $resourceId, $params)
    {
        $model = $this->getFullClassname($model);
        $updatePath = $this->getBaseUri().$model::getWorkspaceResourcePath($workspaceId, $resourceId);
        $curl = $this->createPutRequest($updatePath, $this->loginInfo, $params);

        $response = $this->curlExec($curl);

        return $response;
    }

    function updateModelObject($model, $resourceId, $params)
    {
        $model = $this->getFullClassname($model);
        $params = $this->labelParamKeys($this->getClassname($model), $params);
        $updatePath = $this->getBaseUri().$model::getResourcePath($resourceId);
        $curl = $this->createPutRequest($updatePath, $this->loginInfo, $params);

        $response = $this->curlExec($curl);

        return $response;
    }

    function deleteModel($model, $workspaceId, $resourceId)
    {
        $model = $this->getFullClassname($model);
        $resourcePath = $this->getBaseUri().$model::getWorkspaceResourcePath($workspaceId, $resourceId);
        $curl = $this->createDeleteRequest($resourcePath, $this->loginInfo);

        return $response = $this->curlExec($curl);
    }

    public function deleteStory($id)
    {
        return $this->deleteEntity('Story', $id);
    }

    public function deleteTimeEntry($id)
    {
        return $this->deleteEntity('TimeEntry', $id);
    }


    function deleteEntity($model, $resourceId)
    {
        $model = $this->getFullClassname($model);
        $resourcePath = $this->getBaseUri().$model::getResourcePath($resourceId);
        $curl = $this->createDeleteRequest($resourcePath, $this->loginInfo);

        return $response = $this->curlExec($curl);
    }

    function createPostRequest($url, $accessCredentials, $params)
    {
        $curlHandle = $this->getCurlHandle($url, $accessCredentials);
        $params = http_build_query($params);
        $params = preg_replace('/%5B[0-9]+%5D/simU', '%5B%5D', $params);
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $params);

        return $curlHandle;
    }

    function createPutRequest($url, $accessCredentials, $params)
    {
        $curlHandle = $this->getCurlHandle($url, $accessCredentials);
        $params = http_build_query($params);
        $params = preg_replace('/%5B[0-9]+%5D/simU', '%5B%5D', $params);
        curl_setopt($curlHandle, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $params);

        return $curlHandle;
    }

    function createDeleteRequest($url, $accessCredentials)
    {
        $curlHandle = $this->getCurlHandle($url, $accessCredentials);
        curl_setopt($curlHandle, CURLOPT_CUSTOMREQUEST, 'DELETE');

        return $curlHandle;
    }

    function getMeta(){
        return $this->_last_call_meta;
    }

    private function getBaseUri()
    {
        $url = $this->app_url;
        $hasProtocol = (strpos($url, "http://") !== false || strpos($url, "https://") !== false);
        if(!$hasProtocol){
            $url = 'https://'.$url;
        }
        if(strpos($url, 'api/v1') === false){
            $url = rtrim($url, "/").'/api/v1';
        }
        $url = rtrim($url, "/").'/';
        return $url;
    }

    private function getCurlHandle($url, $accessCredentials)
    {
        if(is_null($accessCredentials)){
            throw new \Exception("Missing Authentication Credentials.  Please call setToken");
        }

        $curlOptions = array
        (
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => array('Authorization: Bearer ' . $accessCredentials),
            CURLOPT_RETURNTRANSFER => TRUE
        );

        $curlHandle = curl_init();
        curl_setopt_array($curlHandle, $curlOptions);
        //curl_setopt($curlHandle, CURLOPT_VERBOSE, true);
        curl_setopt($curlHandle, CURLOPT_CONNECTTIMEOUT, $this->_timeout_after);
        curl_setopt($curlHandle, CURLOPT_TIMEOUT, $this->_timeout_after);

        return $curlHandle;
    }

    public function json2collection($model, $json)
    {
        $model = $this->getFullClassname($model);
        $parsed_json = json_decode($json, true);
        $entities = $parsed_json[$model::$path];
        $this->_last_call_meta = null;
        $collection = array();
        if(!is_null($entities)){
            $this->_last_call_meta = $parsed_json['meta'];
            foreach($entities as $entity)
            {
                $object = new $model();
                foreach($entity as $field => $value)
                {
                    $object->{$field} = $value;
                }
                $collection[] = $object;
            }
        }
        return $collection;
    }

    private function getFullClassname($class)
    {
        return $this->models_namespace . $class;
    }

    private function getClassname($model)
    {
        $parts = explode('\\', $model);
        if (!count($parts))
        {
            return 0;
        }

        return array_pop($parts);
    }

    private function applyFilters($url, $filters= array())
    {
        if (!count($filters))
        {
            return $url;
        }

        $filters_str = $this->filtersToUrl($filters);
        $url .= strpos($url, '?') === false ? '?' : '&';
        return $url . $filters_str;
    }

    private function filtersToUrl($filters)
    {
        return http_build_query($filters);
    }


    private function curlExec($resource)
    {
        $try_number = 1;
        while(true)
        {
            if ($try_number > $this->_retry_times)
            {
                break;
            }

            $response = curl_exec($resource);
            if (curl_errno($resource) != 28) // not timeout error
            {
                break;
            }
            sleep($this->_sleep_on_timeout);
            $try_number++;
        }
        return $response;
    }
}